package com.backbase.assignment.ui

import java.text.SimpleDateFormat
import java.util.*

fun Date.customDateFormat(input: String): String {
    val format1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val format2 = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
    val date = format1.parse(input)
    return if (date != null)
        format2.format(date)
    else
        ""
}