package com.backbase.assignment.ui.popular

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.api.Movie
import com.backbase.assignment.ui.customDateFormat
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.movie_item.view.poster
import kotlinx.android.synthetic.main.popular_movie_item.view.*
import java.util.*

private const val ITEM = 0
private const val LOADING = 1

private const val POSTER_URL = "https://image.tmdb.org/t/p/original/"

class PopularAdapter() : RecyclerView.Adapter<PopularHolder>() {

    private lateinit var listener: (String) -> Unit
    private var moviesList = mutableListOf<Movie>()

    private var isLoadingAdded = false

    constructor(listener: (String) -> Unit) : this() {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularHolder {
        return PopularHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.popular_movie_item, parent, false)
        )
    }

    override fun getItemCount() = moviesList.size

    override fun onBindViewHolder(holder: PopularHolder, position: Int) {
        holder.bind(moviesList[position], listener)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == moviesList.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    private fun add(mc: Movie) {
        moviesList.add(mc)
        notifyItemInserted(moviesList.size - 1)
    }

    fun addAll(mcList: List<Movie>) {
        for (mc in mcList)
            add(mc)
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position: Int = moviesList.size - 1
        moviesList.removeAt(position)
        notifyItemRemoved(position)
    }
}

class PopularHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(movie: Movie, listener: (String) -> Unit) = with(itemView) {
        Glide
            .with(context)
            .load("$POSTER_URL${movie.posterPath}")
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(itemView.poster)

        itemView.movieTitle.text = movie.title
        val rating = movie.voteAverage * 10
        itemView.rating.value(rating.toInt())
        itemView.movieDateRelease.text = Date().customDateFormat(movie.releaseDate)

        setOnClickListener {
            listener(movie.id)
        }
    }
}