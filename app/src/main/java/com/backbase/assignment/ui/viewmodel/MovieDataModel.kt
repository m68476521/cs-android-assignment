package com.backbase.assignment.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.backbase.assignment.ui.api.MovieDetails

class MovieDataModel : ViewModel() {
    lateinit var movie: MovieDetails
}