package com.backbase.assignment.ui.api

import com.backbase.assignment.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {
    companion object {
        fun create(url: String, apiKey: String = ""): MovieService {
            val retrofit = Retrofit.Builder()
                .client(createClient(apiKey))
                .addCallAdapterFactory(RxErrorHandlerAdapter())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(createDateFormatter()))
                .baseUrl(url)
                .build()
            return retrofit.create(MovieService::class.java)
        }

        private fun createClient(apiKey: String): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(RequestInterceptor(apiKey))
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                })
                .build()
        }

        private fun createDateFormatter(): Gson {
            return GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                .create()
        }
    }

    @GET("3/movie/now_playing")
    fun nowPlaying(
        @Query("language") language: String,
        @Query("page") page: String
    ): Single<Movies>

    @GET("3/movie/popular")
    fun popularMovies(
        @Query("language") language: String,
        @Query("page") page: String
    ): Single<Movies>

    @GET("3/movie/{id}")
    fun moviesById(
        @Path("id") id: String
    ): Single<MovieDetails>
}
