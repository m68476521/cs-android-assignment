package com.backbase.assignment.ui.api

class MovieApi(private val movieApi: MovieService) {
    fun nowPlaying(language: String = "en-US", page: String = "undefined") =
        movieApi.nowPlaying(language, page)

    fun popularMovies(language: String = "en-US", page: String = "undefined") =
        movieApi.popularMovies(language, page)

    fun movieById(id: String) = movieApi.moviesById(id)
}
