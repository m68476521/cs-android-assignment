package com.backbase.assignment.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.backbase.assignment.R
import com.backbase.assignment.ui.api.MovieManager
import com.backbase.assignment.ui.movie.MoviesAdapter
import com.backbase.assignment.ui.popular.PaginationScrollListener
import com.backbase.assignment.ui.popular.PopularAdapter
import com.backbase.assignment.ui.viewmodel.MovieDataModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*

private const val PAGE_START = 0

class HomeFragment : Fragment() {
    private val compositeDisposable = CompositeDisposable()

    private lateinit var popularMoviesAdapter: PopularAdapter
    private lateinit var moviesAdapter: MoviesAdapter

    private val movieModel: MovieDataModel by activityViewModels()

    private var loading = false
    private var lastPage = false
    private var totalPages = 0
    private var currentPage = PAGE_START

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moviesAdapter = MoviesAdapter()
        nowPlayingMovies.adapter = moviesAdapter
        nowPlayingMovies.setHasFixedSize(false)

        nowPlayingMovies.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        fetchMovies()
        setupPopularMovies()
    }

    private fun fetchMovies() {
        val disposable = MovieManager.movieApi.nowPlaying()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                moviesAdapter.swapMovies(it.results)
            }, {
                it.printStackTrace()
            })

        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun setupPopularMovies() {
        popularMoviesAdapter = PopularAdapter { getMovie(it) }
        popularMovies.adapter = popularMoviesAdapter
        popularMovies.setHasFixedSize(false)

        val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        popularMovies.layoutManager = linearLayoutManager

        popularMovies.addOnScrollListener(object: PaginationScrollListener(linearLayoutManager) {
            override fun loadMoreItems() {
                loading = true
                currentPage += 1
                loadMoreMovies()
            }

            override val totalPageCount: Int = totalPages

            override val isLastPage: Boolean = lastPage

            override val isLoading: Boolean = loading
        })
        initialLoad()
    }

    private fun getMovie(id: String) {
        val disposable = MovieManager.movieApi.movieById(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                movieModel.movie = it
                val next = HomeFragmentDirections.actionHomeFragmentToDetailsMovie2Fragment()
                requireView().findNavController().navigate(next)
            }, {
                it.printStackTrace()
            })
        compositeDisposable.add(disposable)
    }

    private fun initialLoad() {
        val disposable = MovieManager.movieApi.popularMovies()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                popularMoviesAdapter.addAll(it.results)
                progressBarLoading.visibility = View.GONE
                if (currentPage <= totalPages) popularMoviesAdapter.addLoadingFooter() else lastPage = true
            }, {
                it.printStackTrace()
            })
        compositeDisposable.add(disposable)
    }

    private fun loadMoreMovies() {
        val disposable = MovieManager.movieApi.popularMovies(page = currentPage.toString())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                popularMoviesAdapter.removeLoadingFooter()
                loading = false
                popularMoviesAdapter.addAll(it.results)
                if (currentPage != totalPages) popularMoviesAdapter.addLoadingFooter() else lastPage = true
            }, {
                it.printStackTrace()
            })
        compositeDisposable.add(disposable)
    }
}