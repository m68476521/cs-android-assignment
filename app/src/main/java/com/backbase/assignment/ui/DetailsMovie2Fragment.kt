package com.backbase.assignment.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.backbase.assignment.R
import com.backbase.assignment.ui.api.MovieDetails
import com.backbase.assignment.ui.viewmodel.MovieDataModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_details_movie2.*
import java.util.Date

private const val POSTER_URL = "https://image.tmdb.org/t/p/original/"

class DetailsMovie2Fragment : Fragment() {

    private val movieModel: MovieDataModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details_movie2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showInfo(movieModel.movie)

        backButton.setOnClickListener { it.findNavController().popBackStack() }
    }

    private fun showInfo(movie: MovieDetails) {
        Glide
            .with(requireContext())
            .load("${POSTER_URL}${movie.posterPath}")
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_background)
            .into(poster)

        movieTitle.text = movie.title

        val hour = movie.runtime / 60
        val min = movie.runtime % 60

        val subTitle = "${Date().customDateFormat(movie.releaseDate)} - $hour hr $min min"

        movieDetail.text = subTitle
        description.text = movie.overview
    }
}