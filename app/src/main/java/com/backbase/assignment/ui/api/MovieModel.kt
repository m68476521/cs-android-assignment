package com.backbase.assignment.ui.api

import com.google.gson.annotations.SerializedName

data class Movie(
    val id: String,
    @SerializedName("poster_path")
    val posterPath: String,
    val popularity: Double,
    @SerializedName("vote_count")
    val voteCount: String,
    val video: Boolean,
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    val title: String,
    @SerializedName("vote_average")
    val voteAverage: Double,
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String
)

data class MovieDetails(
    val adult: Boolean,
    @SerializedName("backdrop_path")
    val backdropPath: String,
    @SerializedName("belongs_to_collection")
    val belongsCollection: String,
    val budget: Int,
    val genres: List<Genere>,
    val home: String,
    @SerializedName("imdb_id")
    val imdbId: String,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val id: String,
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("release_date")
    val releaseDate: String,
    val revenue: Int,
    val runtime: Int,
    val status: String,
    @SerializedName("tagline")
    val tagLine: String,
    val title: String,
    val video: Boolean,
    @SerializedName("vote_count")
    val voteCount: String,
    @SerializedName("vote_average")
    val voteAverage: Double
)

data class Movies(
    val results: List<Movie>,
    val page: Int,
    @SerializedName("total_results")
    val totalResults: Int,
    val dates: Dates,
    @SerializedName("total_pages")
    val totalPages: Int
)

data class Dates(
    val maximum: String,
    val minimum: String
)

data class Genere(
    val id: String,
    val name: String
)
