package com.backbase.assignment.ui.api

object MovieManager {
    const val URL = "https://api.themoviedb.org/"

    lateinit var movieApi: MovieApi

    fun setToken(apiKey: String) {
        movieApi = MovieApi(MovieService.create(URL, apiKey))
    }
}
