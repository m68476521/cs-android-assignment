package com.backbase.assignment.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.backbase.assignment.R
import kotlinx.android.synthetic.main.custom_rating.view.*

class RatingView(context: Context, attrSet: AttributeSet) : ConstraintLayout(context, attrSet) {
    init {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_rating, this, false)
        val set = ConstraintSet()
        addView(view)
        setConstraints(view, this, set)

        setAttributes(attrSet)
    }

    private fun setConstraints(view: View, parent: ConstraintLayout, set: ConstraintSet) {
        with(set) {
            set.clone(this)
            set.connect(view.id, ConstraintSet.TOP, parent.id, ConstraintSet.TOP)
            set.connect(view.id, ConstraintSet.START, parent.id, ConstraintSet.START)
            set.connect(view.id, ConstraintSet.END, parent.id, ConstraintSet.END)
            set.connect(view.id, ConstraintSet.BOTTOM, parent.id, ConstraintSet.BOTTOM)
        }
    }

    private fun setAttributes(attrSet: AttributeSet) {
        val attributes = context.obtainStyledAttributes(attrSet, R.styleable.RatingView)
        val value = attributes.getInt(R.styleable.RatingView_value, 0)

        progressBar.progress = value

        attributes.recycle()
    }

    fun value(value: Int) {
        progressBar.progress = value
        progressValue.text = value.toString()
        this.invalidate()
    }
}
