# Android Assignment CS
This is a placeholder README file with the instructions for the assingment. We expect you to build your own README file.

## Instructions

You can find all the instrucrtions for the assingment on [Assingment Instructions](https://docs.google.com/document/d/1zCIIkybu5OkMOcsbuC106B92uqOb3L2PPo9DNFBjuWg/edit?usp=sharing).

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work with small commits, it help us to see how you improve your code :)

# Miguel Orozco's solution

## Solution description

Use of libraries Retrofit, okhttp, gson and RxJava. Create a client http to handle the call to the endpoints(API themoviedb). I create a singleton to initialize the client. 
Once the client is ready, the app can easily hits the endpoints to fetch the data. The is a basic Api exception file that can be used to handles the id.

Use of library architecture components. Navigation components is a powerful tool to handles the navigation between fragments.

Use of Glide. This library is used to show images in a ImageView(this particular case)

## Image cache solution 
I saw a big performance change once that I change the library from Picasso to Glide, this allows me to scroll the list of movies smoothly.

## Navigation components
In this project, the architecture used is one Activity to many fragments by using Navigation components.

## Movies listed
For both carousels of movies, the application is using recycler views. This allows the app to scroll horizontal and vertical.

## TODO
* Change the style for the rating depending of the value.
* Add unit tests
* Add duration of movie at the recycler item view. Note: the endpoint in not retrieving it.


